# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProjectTask(models.Model):
    _inherit = "project.task"

    #@api.multi
    #def add_project_followers(self):
    #    for task in self:
    #        project_followers = task.project_id.message_follower_ids
    #        for follower in project_followers:
    #            task.message_subscribe(partner_ids=follower.partner_id.ids, subtype_ids=follower.subtype_ids.ids)

    #@api.multi
    #def add_project_followers(self):
    #    for task in self:
    #        partner_ids = task.project_id.message_follower_ids.ids
    #        task.message_subscribe(partner_ids)

    #@api.multi
    #def add_project_followers(self):
    #    for follower in project_id.message_follower_ids:
    #        if follower not in task_id.message_follower_ids:
    #            task_id.message_follower_ids = [(6, 0, project_id.message_follower_ids.ids)]

    @api.multi
    def add_project_followers(self):
        for task in self:
            for follower in task.project_id.message_follower_ids:
                if follower not in task.message_follower_ids:
                    task.message_follower_ids = [(4, follower.id)]


    @api.multi
    def clear_assigned_to(self):
        for i in self:
            i.user_id = False
